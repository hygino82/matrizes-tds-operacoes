package application;

import util.Matriz;

public class Program {

    public static void main(String[] args) {
        double[][] a = {{1, 4, 7}, {4, 2, 9}};
        double[][] b = {{2, 9, 1}, {7, 2, 3}};
        String[][] nomes = {{"Ana", "Maria", "Lucio"}, {"Roberto", "Juvenal", "Jurandi"}};

        double[][] soma = Matriz.soma(a, b);
        String[][] somaCaracteres = Matriz.soma(nomes, a);

        System.out.println("Matriz dos nomes");
        Matriz.escrevamatriz(nomes);
        System.out.println("----------------------------------------");

        System.out.println("Matriz A");
        Matriz.escrevamatriz(a);
        System.out.println("----------------------------------------");

        System.out.println("Matriz B");
        Matriz.escrevamatriz(b);
        System.out.println("----------------------------------------");

        System.out.println("Soma numérica");
        Matriz.escrevamatriz(soma);
        System.out.println("----------------------------------------");

        System.out.println("Soma caracteres");
        Matriz.escrevamatriz(somaCaracteres);
    }
}
