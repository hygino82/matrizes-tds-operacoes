package util;

import java.util.InputMismatchException;

public class Matriz {

    public static double[][] soma(double[][] a, double[][] b) {
        if ((a.length == b.length) && (a[0].length == b[0].length)) {
            double soma[][] = new double[a.length][b[0].length];
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < b[0].length; j++) {
                    soma[i][j] = a[i][j] + b[i][j];
                }
            }
            return soma;
        }
        throw new InputMismatchException("Impossível somar as matrizes");
    }

    public static String[][] soma(String[][] a, double[][] b) {
        if ((a.length == b.length) && (a[0].length == b[0].length)) {
            String soma[][] = new String[a.length][b[0].length];
            for (int i = 0; i < a.length; i++) {
                for (int j = 0; j < b[0].length; j++) {
                    soma[i][j] = a[i][j] + b[i][j];
                }
            }
            return soma;
        }
        throw new InputMismatchException("Impossível somar as matrizes");
    }

    public static void escrevamatriz(double[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.printf("%.5f\t\t", matriz[i][j]);
            }
            System.out.println();
        }
    }

    public static void escrevamatriz(String[][] matriz) {
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                System.out.printf("%s\t\t", matriz[i][j]);
            }
            System.out.println();
        }
    }
}
